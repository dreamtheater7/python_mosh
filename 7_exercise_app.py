# !/usr/bin/env python3
# -*-coding:cp1252-*-


def main():
    print("this is exercise 7.")

    # classes are blueprints and by giving a name to the class
    # we create a Object - this Object can inheritent from other Objects
    # as well as others can inheritent from this very Object called Vehicle
    # v1 which is an isntance of the class Vehicle
    # functions in class have by convention at least one parameter
    # which is self (it can acctually have any name) and by doing so the function
    # becomes a class method - a method of the class Object
    # and the parameters become attributes - describing the method
    # and hence beeing a feature or attribute of the class itself
    # self could be compared with "this" in c++ or an pointer
    # it got the same address as the argument pass to the attributes in the methods
    # therefor by using the dot operator the, e.g. "self.speed" can be used everythere
    # in the class
    class Vehicle:

        # global class variable
        discount = 0.8

        # Constructor
        def __init__(self, title: str, speed: float, wheels: int, price: float):
            assert speed >= 160, f"speed {speed} is to low."
            assert wheels >= 2, f"to few wheels {wheels}."

            self.model_name = title
            self.how_fast = speed
            self.number_of_wheels = wheels
            self.price = price

            print(f"address of self is {id(self)}")

        def club_member(self, member: bool):
            self.member = member

        # class method or function definition
        # by defining a constructor no need of attributes other than self
        # like def printing_stuff(self, speed):
        def printing_stuff(self):
            # print("What a fantastic car it is!")
            # self.speed = speed
            if self.how_fast >= 290 and self.number_of_wheels > 2:
                print("this is a coupe")
            elif self.number_of_wheels == 2:
                print("this is a motorbike.")

        def calculate_new_price(self):
            self.price *= self.discount

    # creating instance v1 of Vehicle which is an object of type class
    # with constructor:
    # by using a consturctor - arguments can be passed in the instance declaration
    v1 = Vehicle("Puma", 310, 4, 35000.00)
    # after passing the attributes in the instance declaration
    # the way of getting access to the variable is via the variable declared
    # with self in the constructor - self.model_name
    print(f"model {v1.model_name}")
    print(v1.printing_stuff())
    print(f"top speed is {v1.how_fast}")
    c_member = input("are you a club member (y or n): ").lower()
    if c_member == 'y':
        v1.club_member(True)
    else:
        v1.club_member(False)
    # print(v1.member)
    if not v1.member:
        print(f"the price is {v1.price}")
    elif v1.member:
        print(f"the original price is {v1.price}")
        v1.calculate_new_price()
        print(f"but for club members the price is {v1.price}")
    # without constructor:
    # v1 = Vehicle()
    # v1.printing_stuff(210)
    # print(v1.speed)

    print(f"address of Vehicle is {id(Vehicle)}")
    print(f"address of v1 is {id(v1)}")


if __name__ == "__main__":
    main()
