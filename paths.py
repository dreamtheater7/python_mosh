# !/usr/bin/env python3
# -*-coding:cp1252-*-
from pathlib import Path


def main():
    print("from path lesson.")

    # Path("C: \\Program Files")
    # Path(r"C: \Program Files\Microsoft")
    # Path()
    # Path("ecommerce/__init__.py")
    # Path() / "ecommerce" / "__init__.py"
    # Path.home()
    path = Path("ecommerce/__init__.py")
    path.exists()
    path.is_file()
    path.is_dir()
    print(path.name)
    print(path.stem)
    print(path.suffix)
    print(path.parent)
    path = path.with_name("file.txt")
    path = path.with_suffix(".txt")
    print(path)
    print(path.absolute())

    print("YEAH")


if __name__ == "__main__":
    main()
