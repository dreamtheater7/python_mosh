# !/usr/bin/env python3
# -*-coding:cp1252-*-


def main():
    print("this is exercise.")

    class Member:
        def __init__(self, member: bool):
            self.is_member = member

        def defining_member(self):
            if self.is_member == "y":
                self.is_member = True
            else:
                self.is_member = False

        def draw_member(self):
            print(f"{self.is_member}")

    class Vehicle(Member):

        discount_rate = 0.8

        def __init__(self, title: str, speed: float, wheels: int, price: float):
            assert speed >= 190, f"the speed {speed} is to slow."
            assert wheels >= 2, f"wheels {wheels} to few."

            self.model_name = title
            self.how_fast = speed
            self.number_of_wheels = wheels
            self.price = price

        def draw(self):
            print("printing.")
            if self.how_fast >= 290 and self.number_of_wheels > 2:
                print(f"model {self.model_name} is a coupe.")
            else:
                print(f"model {self.model_name} is a motorbike.")

        def calculate_new_price(self):
            self.price *= self.discount_rate

    v1 = Vehicle("Puma", 310, 4, 35777.90)
    v1.draw()
    print(f"car is called {v1.model_name}")
    print(f"max speed is {v1.how_fast}")
    print(f"the price is {v1.price}")
    # v1.is_member = input("are club member: (y/n)").lower()
    # v1.defining_member()
    # v1.draw_member()
    # if v1.is_member:
    #     v1.calculate_new_price()
    #     print(f"club members price is {v1.price}")
    # else:
    #     print(f"price of {v1.model_name} is {v1.price}")



if __name__ == "__main__":
    main()
