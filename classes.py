# !/usr/bin/env python3
# -*-coding:cp1252-*-


def main():
    # class attributes - they are the same across all instances
    print("this is a class program.")

    class Point:
        default_color = "blue"
        # constructor

        def __init__(self, x, y):
            # instance attributes - they can be different for all pointobjects
            self.x = x
            self.y = y

        def __str__(self):
            return f"({self.x}, {self.y})"

        @classmethod
        def zero(cls):
            return cls(1, 2)

        def draw(self):
            # print("draw")
            print(f"Point {self.x}, {self.y}")

    Point.default_color = "yellow"
    point = Point(1, 2)  # creating an Point object called point
    print(type(point))
    print(isinstance(point, Point))
    point.draw()
    print(point.x)
    print(point.y)
    print(point.default_color)
    print(Point.default_color)
    point = Point.zero()
    print(point)
    print(str(point))

    # another = Point(3, 4)
    # another.draw()
    # print(another.default_color)


if __name__ == "__main__":
    main()
