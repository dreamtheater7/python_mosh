# !/usr/bin/env python3
# -*-coding:cp1252-*-
# built in exceptions https://docs.python.org/3/library/exceptions.html

from timeit import timeit


def main():
    print("this is a exceptions program.")

    # numbers = [1, 2]
    # print(numbers[3])

    # try:
    #     age = int(input("Age: "))
    # except ValueError:
    #     print("You didn't enter a valid age.")
    # else:
    #     print("No exception made.")

    # print("Execution Continues.")
    # print(age)

    # try:
    #     age = int(input("Age: "))
    # except ValueError as ex:
    #     print("You didn't enter a valid age.")
    #     print(ex)
    #     print(type((ex)))
    # else:
    #     print("No exception made.")

    # print("Execution Continues.")
    # print(age)

    # try:
    #     # file = open("app.py") # which needs file.close()
    #     # better is
    #     with open("app.py") as file:
    #         print("File opened.")

    #     age = int(input("Age: "))
    #     xfactor = 10 / age
    # except (ValueError, ZeroDivisionError):
    #     print("You didn't enter a valid age.")
    # # except ZeroDivisionError:
    # #     print("You didn't enter a valid age.")
    # else:
    #     print("No exception made.")
    # # the finally clause is to release external resources
    # # not needed with "with"
    # # finally:
    # #     file.close()
    # print("Execution Continues.")
    # # print(age)
    # # print(xfactor)

    code1 ="""
    def mplampla(mpla):
        pass
    # def calculate_xfactor(age):
        # if age <= 0:
        #     raise ValueError("age can't be 0 or less.")
        # return 10 / age
        # pass

    # try:
    #     calculate_xfactor(-1)
    # except ValueError as error:
    #     # print(error)
    #     pass
    """

    # code2 = """
    # def calculate_xfactor(age):
    #     if age <= 0:
    #         return None
    #     return 10 / age

    # xfactor = calculate_xfactor(-1)
    # if xfacotr == None:
    #     pass
    # """

    print("first code=", timeit(code1, number=10000))
    # print("second code=", timeit(code2, number=10000))


if __name__ == "__main__":
    main()
