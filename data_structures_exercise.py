# !/usr/bin/env python3
# -*-coding:cp1252-*-
# write a program to find the most repeated character in the text.


def main():
    #print("this is data exercise.")

    sentence = "This is a common interview question."
    print(sentence)

    # this was my solution and it worked
    # max = 0
    # max_character = []
    # for i in range(len(sentence)):
    #     cnt = 0
    #     for j in range(len(sentence)):
    #         if sentence[j] == sentence[i]:
    #             cnt += 1
    #         if cnt > max:
    #             max = cnt
    #             max_character.append(sentence[j])

    # max_char = max_character.pop()
    # print(f"the cnt is {max} and character is {max_char}")
    # print(max_character)

    # solution of the course
    char_sequency = {}
    # print(char_sequency)
    for char in sentence:
        # print(char)
        # print(char_sequency)
        if char in char_sequency:
            char_sequency[char] += 1
            # print(char_sequency)
            # break
        else:
            char_sequency[char] = 1
            # print(char_sequency)
            # break
    result = (sorted(char_sequency.items(),
                     key=lambda kv: kv[1], reverse=True))
    print(result[0])

    # by adding a value to not existing key in the dictionary
    # python will add the key value pair
    # dictio = {}
    # dictio["John Petrucci"] = "777 999 333"
    # print(dictio)


if __name__ == "__main__":
    main()
