#!/usr/bin/env python3
#-*-coding:cp1252-*-
import sys
import random


def main():
    print(" ", sys.argv)
    print("Progressive Music.")

    pi = 3
    n = 2
    sign = 1

    def fib_rec(idx):
        if idx < 2:
            return idx
        else:
            return fib_rec(idx - 2) + fib_rec(idx - 1)

    inp = 8
    result_rec = fib_rec(inp)
    print(f"the fibonacci number of {inp} is {result_rec}")

    def fib_iteration(idx):
        seq = [0, 1]
        for i in range(idx):
            seq.append(seq[-2] + seq[-1])
        return seq[-2]

    inp_i = 8
    result_iteration = fib_iteration(inp_i)
    print(f"the fibonacci number of {inp_i} is {result_iteration}")

    for i in range(1000001):
        # sign = sign * (-1)
        pi = pi + (sign * (4 / ((n) * (n + 1) * (n + 2))))
        sign = sign * (-1)
        n = n + 2

    print(f"the pi number is {pi}")

    pi = 3
    n = 2
    sign = 1

    def pi_rec(i: int, pi: float, n: int, sign):
        if i < 1:
            return pi
        else:
            pi = pi + (sign * (4 / ((n) * (n + 1) * (n + 2))))
            sign = sign * (-1)
            return pi_rec(i - 1, pi, n + 2, sign)

    result_pi = pi_rec(100, pi, n, sign)
    print(f"the pi number is {result_pi}")

    random_number = random.random()

    # PI = 4 * N * (r <= 1) / N
    # N are tuples of two random numbers between 0 and 1
    # and r = sqrt(x^2 + y^2) x and y are random numbers

    # pi = 4 *
    nums = tuple(random.random() for _ in range(2))
    x = nums[0] * nums[0]
    y = nums[1] * nums[1]
    num = (x + y)**(1 / 2)
    print(num)
    res = (4 * num) / num
    print(res)

    def T(n):
        # using some simple math for arithmetic progressions
        Tn = (4 / (2 * n - 1) * (-1)**(1 - n))
        if n == 0:
            return 0
        if n == 1:
            return 4
        else:
            return Tn + T(n - 1)

    result_p = T(990)
    print(result_p)




if __name__ == "__main__":
    main()
