# !/usr/bin/env python3
# -*-coding:cp1252-*-

def main():
    print("this is exercise.")

    class Vehicle:

        discount = 0.8

        def __init__(self, title: str, speed: float, doors: int, price: float):
            assert speed >= 210, f"speed {speed} is to low."

            self.model_name = title
            self.how_fast = speed
            self.number_of_doors = doors
            self.price = price

        def club_member(self, member: bool):
            self.is_member = member
            return self.is_member

        def draw(self):
            print("this is a method.")
            if self.how_fast >= 290 and self.number_of_doors <= 3:
                print("this model is a coupe.")
            else:
                print("this model is a limusine.")

        def calculate_new_price(self):
            self.price *= self.discount

    v1 = Vehicle("Puma", 310, 3, 35000.90)
    print(f"the model name is {v1.model_name}")
    print(f"max speed of this model is {v1.how_fast}")
    print(f"original price is {v1.price}")
    v1.draw()
    member = input("are you a club memeber (y/n): ").lower()
    v1.club_member(member)
    if member == "n":
        print(f"the price is: {v1.price}")
    else:
        v1.calculate_new_price()
        print(f"the price for club members is: {v1.price}")


if __name__ == "__main__":
    main()
