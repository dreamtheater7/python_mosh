# !/usr/bin/env python3
# -*-coding:cp1252-*-


def main():
    print("building an custom container.")

    # this is an implementation of an dictionary via class object
    # data structures are list, set, tuples and dictionaries
    # which can hold different data types like integer, float, string
    # therefore data structures are containers
    # container is an object that holds other objects
    # since everything in Python is an object
    # a variable (data type: integer) with a value is an object
    # with other words - data structures are containers that organize and group
    # data according to type. The data structure differ based on mutability
    # and order.
    # this means that TagCloud is an implementation of an container with
    # the data structure of an dictionary
    # now the underlying dictionary is initialized in the constructor
    class TagCloud:

        # initialization of dictionary
        # changing the self.tags to self.__tags to make it a private member
        # this will avoid the crash if try to access the underlying dictionary
        # as mentioned outside the class.
        def __init__(self):
            self.__tags = {}

        def add(self, tag):
            self.__tags[tag.lower()] = self.__tags.get(tag.lower(), 0) + 1

        def __getitem__(self, tag):
            return self.__tags.get(tag.lower(), 0)

        def __setitem__(self, tag, count):
            self.__tags[tag.lower()] = count

        def __len__(self):
            return len(self.__tags)

        def __iter__(self):
            return iter(self.__tags)

    cloud = TagCloud()
    # print(len(cloud))
    # print(cloud.__tags)
    # print(cloud.__dict__)
    cloud.add("python")
    cloud.add("c")
    cloud.add("javascript")
    cloud.add("c++")
    cloud.add("c++")
    cloud.add("c++")
    cloud.add("django")
    print(cloud.tags)
    print(cloud.tags["c++"])
    print(cloud["c++"])
    # access to the implementation of the container
    # the difference here to the this attempt (print(cloud.tags["PYTHON"]))
    # is that in the case of print(cloud["PYTHON"]) it will be transformed
    # to lowercase and this key exist in the dictionary
    print(cloud["PYTHON"])
    # access to the underlying dictionary
    # in this case every key is stored lower case
    # by trying to match it will crash with error message KeyError: 'PYTHON'
    # because no such key is stored
    print(cloud.tags["PYTHON"])
    # to avoid this error message or the crash we can set a private member
    # like __tags
    # now the erroe message is like
    # AttributeError: 'TagCloud' object has no attribute 'tags'
    # this protects the user to accidentically access the underlying dictionary.


if __name__ == "__main__":
    main()  # this is the main program.
