# !/usr/bin/env python3
# -*-coding:cp1252-*-


def main():
    print("this is a property example from the lesson.")

    class Product:

        # constructor - initializing attributes
        def __init__(self, price):
            # self.__price = price
            # self.set_price(price)
            self.price = price

        # decorator
        @property
        def price(self):
            # def get_price(self):
            return self.__price

        # decorator
        @price.setter
        def price(self, value):
            # def set_price(self, value):
            if value < 0:
                raise ValueError("Price can not be negative.")
            self.__price = value

        # using property
        # price = property(get_price, set_price)

    product = Product(10)
    # print(product.__price)
    product.price = -1
    print(product.price)


if __name__ == "__main__":
    main()
