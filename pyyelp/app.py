#!/usr/bin/env python3
#-*-coding:cp1252-*-
import sys
import requests
import config


def main():
    print(" ", sys.argv)

    url = "https://api.yelp.com/v3/businesses/search"
    headers = {
        "Authorization": "Bearer " + config.api_key
    }
    params = {
        "term": "Barber",
        "location": "NYC"
    }
    response = requests.get(url, headers=headers, params=params)
    # print(respond.text)
    businesses = response.json()["businesses"]
    # print(businesses)
    # [item for item in list]
    names = [business["name"]
             for business in businesses if business["rating"] > 4.5]
    # for business in businesses:
    #     print(business["name"])
    print(names)


if __name__ == "__main__":
    main()
