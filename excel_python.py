#!/usr/bin/env python3
# -*-coding:cp1252-*-
import sys
import openpyxl


def main():
    print(" ", sys.argv)

    openpyxl.Workbook()
    wb = openpyxl.load_workbook("transactions.xlsx")
    print(wb.sheetnames)

    sheet = wb["Sheet1"]

    # wb.create_sheet("Sheet2", 0)
    # wb.remove_sheet(sheet)

    cell = sheet["a1"]
    print(cell.value)
    # cell.value = 1 # changing the value of the cell A1
    print(cell.row)
    print(cell.column)
    print(cell.coordinate)
    sheet.cell(row=1, column=1)
    print(sheet.max_row)
    print(sheet.max_column)

    for row in range(1, sheet.max_row + 1):
        for column in range(1, sheet.max_column + 1):
            cell = sheet.cell(row, column)
            print(cell.value)

    print("\n")
    column = sheet["a"]
    cells = sheet["a:c"]
    print(column)
    print(cells)
    cells = sheet["a1:c3"]
    print(cells)
    cells = sheet[1:3]
    print(cells)

    sheet.append([1, 2, 3])
    # sheet.insert_rows()
    # sheet.insert_columns()
    # sheet.delete_rows()
    # sheet.delete.columns()

    wb.save("transactions2.xlsx")


if __name__ == "__main__":
    main()
