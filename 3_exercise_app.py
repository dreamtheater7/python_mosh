# !/usr/bin/env python3
# -*-coding:cp1252-*-


def main():
    print("exercise")

    # Pascal naming convention - first letter of any other word is uppercase
    class Vehicle:
        pay_rate = 0.8

        def __init__(self, title: str, speed: float, wheels: int, doors: int, price: float):
            assert speed > 180, f"none of our models it that {speed} slow."
            assert wheels > 1, f"none of our vehicles has {wheels} wheels."
            assert doors > 1, f"none of our vehicles has {doors} doors."

            self.max_fast = speed
            self.number_of_wheels = wheels
            self.number_of_doors = doors
            self.model_name = title
            self.price = price

        def drive(self):
            if self.max_fast > 310 and self.number_of_doors <= 3:
                print(f"this car is a fast car! ")
            elif self.max_fast < 260:
                print("this is car is a regular car.")
            # print(self.how_fast)

        def calculate_new_price(self):
            self.price *= self.pay_rate

    v1 = Vehicle("Lepard", 390, 4, 3, 55000)
    # v1.drive = 210
    # print(f"v1.drive is {v1.drive}")
    print(f"the number of wheels are {v1.number_of_wheels}")
    print(f"the number of doors are {v1.number_of_doors}")
    print(f"your max speed is {v1.max_fast}")
    print(f"the price is {v1.price}")
    v1.calculate_new_price()
    print(f"but the new price is {v1.price}")
    print(f"this car is called the '{v1.model_name}'")
    # print(isinstance(v1, Vehicle))
    print(v1.drive())

    v2 = Vehicle("Puma", 310, 4, 5, 35000)
    print(f"this car is called the '{v2.model_name}'")
    print(f"the max speed is {v2.max_fast}")
    print(
        f"and it comes with {v2.number_of_wheels} wheels and {v2.number_of_doors} doors.")
    print(v2.drive())

    v3 = Vehicle("Tiger", 190, 4, 5, 15000)
    print(f"this car is called the '{v3.model_name}'")
    print(f"the max speed is {v3.max_fast}")
    print(
        f"and it comes with {v3.number_of_wheels} wheels and {v3.number_of_doors} doors.")
    print(v3.drive())


if __name__ == "__main__":
    main()
