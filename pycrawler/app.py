#!/usr/bin/env python3
# -*-coding:cp1252-*-
import sys
import requests
from bs4 import BeautifulSoup


def main():
    print(" ", sys.argv)

    response = requests.get("https://stackoverflow.com/questions")
    soup = BeautifulSoup(response.text, "html.parser")

    questions = soup.select(".question-summary")
    print(questions)


if __name__ == "__main__":
    main()
