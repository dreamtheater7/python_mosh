# !/usr/bin/env python3
# -*-coding:cp1252-*-


def main():
    print("this is fizz buzz program.")

    def fizz_buzz(inp):
        if (inp % 3 == 0) and (inp % 5 == 0):
            return "Fizz Buzz"
        if inp % 5 == 0:
            return "Fizz"
        if inp % 3 == 0:
            return "Buzz"
        if inp % 3 != 0 or inp % 5 != 0:
            return inp

    result = fizz_buzz(30)
    print(f"{result}")

if __name__ == "__main__":
    main()
