# !/usr/bin/env python3
# -*-coding:cp1252-*-


def main():
    # print("this is an inheritance program.")

    # Animal: Parent, Base - Class
    class Animal:
        def __init__(self):
            self.age = 1

        def eat(self):
            print("eat")

    # Mammal: Child, Sub - Class
    class Mammal(Animal):
        # def eat(self):
        #     print("eat")

        def walk(self):
            print("walk")

    class Fish(Animal):
        # def eat(self):
        #     print("eat")

        def swim(self):
            print("swim")

    m = Mammal()
    m.eat()
    print(m.age)
    print(isinstance(m, Mammal))

    # v1 = 9
    # v2 = 11

    # print(id(v1))
    # print(id(v2))


if __name__ == "__main__":
    main()
