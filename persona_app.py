# !/usr/bin/env python3
# -*-coding:cp1252-*-
# this for testing mosh code


def main():
    # print("testing mosh's code snippets.")

    # age = 23

    # if age >= 18:
    #     message = "Eligible"
    # else:
    #     message = "Not Eligible"

    # print(message)

    # age = 17
    # message = "Eligible" if age >= 18 else "Not Eligible"
    # print(message)

    # for number in range(0, 10, 2):
    #     print("Attempt", number + 1, (number + 1) * ".")

    # is_password = False
    # is_username = True
    # for i in range(3):
    #     if not is_username or not is_password:
    #         print("Access denied.")
    #     else:
    #         print("Access.")
    #         break
    # else:
    #     print("after 3 failed attempt - you locked out.")
    # print("You are logged in.")

    # number = 100
    # while number > 0:
    #     print(number)
    #     number //= 2

    # # first and last in the function definition are the
    # # Parameters !!!
    # def func_person(first, last):
    #     print(f"{first} {last}")

    # # "John" and "Petrucci" in the function call are the
    # # Arguments !!!
    # func_person("John", "Petrucci")

    # # the function call give the function's parameters the arguments

    # # Vehicle is an Object from type class
    # # Parent Class
    # class Vehicle:
    #     def __init__(self, wheels, doors, color):
    #         self.wheels = wheels
    #         self.doors = doors
    #         self.color = color

    # # Child Class
    # class Car(Vehicle):
    #     pass

    # # myVehicle is an instance of the Class Vehicle
    # # 4, 5, "blue" are Attributes of the instance which will
    # # be given to the Object to describe and distinguish the instance
    # # from other instances.
    # # In other wordes Attributes are the Arguments which we give to a function
    # # Of course every Instance of an Object has one ore more
    # # attributes that describes and defines it.
    # myVehicle = Vehicle(4, 5, "blue")
    # print(myVehicle.wheels)

    # myCar = Car(4, 3, "green")
    # print(myCar.doors)

    # lets say that class is the blue print of our Object Vehicle

    # Class is describing the model of an Object with certain attributes
    # these attributes are the characteristics of the Object
    # every instance is an copy of the Object with probably different arguments
    # making it unique from other copies / instances of the same Object
    # in python you can dynamically add / alter(?) attributes in the Object

    # every function definition in a class becomes a method by convention by
    # giving the function the parameter "self"
    # ??? The Object itself is passed in argument ???
    # in other words - the set_drive is an attribute of the class and
    # this attribute holds an function (class method)

    # every method is an attribute of the class that describes the Object
    # and every attribute (parameter) of an function describes the method

    # self is the Object itself passed as argument through the instance
    class Vehicle:

        def set_drive(self, speed):
            # in c it would be like:
            # int v = 7;
            # int *p;
            # p = &v like self.how_fast = speed
            self.how_fast = speed
            print(f"how_fast {self.how_fast}")
            print(f"address of self.how_fast {id(self.how_fast)}")
            print(f"address of speed {id(speed)}")
            print(f"speed {speed}")
            print(self.how_fast is speed)

        def set_wheels(self, number):
            self.number_of_wheels = number
            print(
                f"address of self.number_of_wheels {id(self.number_of_wheels)}")

    # v is an instance of the Object Vehicle
    v = Vehicle()
    print(f"address of v.set_drive(210) {id(v.set_drive(210))}")
    print("**********************************************************")
    print(f"address of v.set_wheels(4) {id(v.set_wheels(4))}")

    print(f"v.how_fast is {v.how_fast}")
    print(f"address of v.how_fast is {id(v.how_fast)}")
    print(f"v.number_of_wheels is {v.number_of_wheels}")

    print(f"address of class Vehicle {id(Vehicle)}")
    print(f"address of instance v.set_drive {id(v.set_drive)}")

    print(f"address of instance v {id(v)}")


if __name__ == "__main__":
    main()
