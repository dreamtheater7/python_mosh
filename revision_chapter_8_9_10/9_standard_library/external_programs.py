# !/usr/bin/env python3
# -*-coding:cp1252-*-
#import sys
import subprocess


def main():
    #print("main ", sys.argv)

    # completed = subprocess.run(["ls", "-l"],
    #                           capture_output=True,
    #                           text=True)
    # completed = subprocess.run(["python", "other.py"],
    #                            capture_output=True,
    #                            text=True,
    #                            check=True)
    # subprocess.run(["git", "commit", "-m", "'Initial commit.'"])
    # subprocess.run(["git", "push"])

    # print("stdout", completed.stdout)
    # print(completed.stdout)

    # subprocess.run(["git", "status"], check=True)

    working_day = input("workind day start/finish (s/f): ").lower()

    if working_day == "s":
        # this could morning routine
        subprocess.run(["git", "fetch"])
        subprocess.run(["git", "pull"])
    elif working_day == "f":
        # this could be an automated process finishing working day
        subprocess.run(["git", "add", "."])
        data = input("commit message > ")
        subprocess.run(["git", "commit", "-m", data])
        subprocess.run(["git", "push"])


if __name__ == "__main__":
    main()
