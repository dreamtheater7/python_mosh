# !/usr/bin/env python3
# -*-coding:cp1252-*-
import sys
# you can either import the entire module as an object
# from sales import calc_shipping, calc_tax
# directory has changed to subdirectory
# from ecommerce.sales import calc_shipping, calc_tax
# or you can import specific objects from that module.
# import sales
# import ecommerce.sales
# from ecommerce import sales

from ecommerce.shopping import sales


def main():
    print(" ", sys.argv)

    # from sales import calc_shipping, calc_tax
    # calc_shipping()
    # calc_tax()

    # import sales
    # sales.calc_shipping()
    # sales.calc_tax()

    # by adding __init__.py to ecommerce folder -
    # Python will treat the ecommerce folder as package
    # the file path to sales.py has changed since now it is
    # in the folder of ecommerce
    # it is in the subdirectory and can be found via __init__.py
    # because it is now mapped to the directory and the module mapped to the file
    # import ecommerce.sales
    # ecommerce.sales.calc_tax()
    # ecommerce.sales.calc_shipping()

    # from ecommerce.sales import calc_shipping, calc_tax
    # calc_tax()
    # calc_shipping()

    # from ecommerce import sales
    # sales.calc_shipping()
    # sales.calc_tax()

    # print(sys.path)

    sales.calc_shipping()



if __name__ == "__main__":
    main()
