# !/usr/bin/env python3
# -*-coding:cp1252-*-
import sys
from ecommerce.shopping import sales
from ecommerce.customer import contact


def main():
    print("in main_r8app  ", sys.argv)

    print("this is before method call.")

    # sales.calc_shipping()
    sales.calc_shipping()
    contact.contacts()

    print("this is after method call.")

    print(dir(sales))
    print(dir(contact))
    print(sales.__name__)
    print(sales.__file__)
    print(sales.__package__)
    print(contact.__name__)
    print(contact.__file__)
    print(contact.__package__)


if __name__ == "__main__":
    main()
