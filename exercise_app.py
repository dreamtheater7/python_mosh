# !/user/bin/env python3
# -*-coding:cp1252-*-

def main():
    print("this is exercise.")

    cnt = 0
    for i in range(1, 10):
        if i % 2 == 0:
            print(i)
            cnt += 1

    print(f"we have {cnt} even numbers.")


if __name__ == "__main__":
    main()
