# !/usr/bin/env python3
# -*-coding:cp1252-*-

def main():
    print("this is a debuging program.")

    def multiple(*numbers):
        res = 1
        for number in numbers:
            res *= number
        return res

    print("Start")
    result = multiple(2, 3, 4, 5)
    print(f"result is {result}")


if __name__ == "__main__":
    main()
