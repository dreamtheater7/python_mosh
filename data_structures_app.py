# !/usr/bin/env python3
# -*-coding:cp1252-*-

from collections import deque
from array import array
from sys import getsizeof


def main():
    print("this is the 5th chaptor - data structures.")

    letters = ["a", "b", "c", "d"]
    students = ["max", "eric", "thomas"]
    # using the * we can multiply the item in the list by the number after the star (*)
    zeros = [0] * 5
    # combined a list of numbers and letters
    combined = zeros + letters
    numbers = list(range(20))
    chars = list("Hello World")
    len_of_chars = len(chars)
    chars_join = " ".join(chars)
    #chars.split = chars_join.split(" ")

    numbers = list(range(20))
    print(numbers[2::2])  # print only even numbers
    print(numbers[::-1])  # reverse the list

    # print(combined)
    # print(numbers)
    # print(chars)
    # print(f"len of chars {len_of_chars}")
    # print(chars_join)
    # print(letters[0])  # first item in the list
    # print(letters[-1])  # last item in the list
    # letters[0] = "A"  # change the first letter in the list
    # print(letters)
    # print(letters[::2])  # step by 2 - print every second item in the list

    numbers = [1, 2, 3]
    first, second, third = numbers  # list unpacking
    # must be always even variables left as right
    numbers_n = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11]
    first, second, *others = numbers_n  # unpacking first 2 items in first 2 variable
    # but the other variables in a list called 'others'
    first, *other, last = numbers_n  # unpacking first item in first variable and
    # the oterher items in the list "item" and the last item in the variable "last"
    print(numbers)
    print(first)

    for i in range(len(letters)):
        print(i, letters[i])

    print("\n")

    for letter in enumerate(letters):
        print(letter)

    print("\n")

    for letter in enumerate(letters):
        print(letter[0], letter[1])

    print("\n")
    print("unpacking lists")
    # items = [0, "a"]
    # index, letters = items
    # print(items)
    for index, letter in enumerate(letters):
        print(index, letter)

    # add
    letters.append("e")
    print(letters)
    letters.insert(0, "-")
    print(letters)

    # remove
    letters.pop()  # delete specific item
    letters.pop(0)
    letters.remove("b")
    del letters[1]
    print(letters)
    del letters[0:]  # delete a range of items
    print(letters)
    letters.append("a")
    letters.append("b")
    letters.append("c")
    print(letters)
    letters.clear()  # delete everything
    print(letters)

    # finding items
    letters_new = ["b", "a", "n", "n", "a", "n", "a"]
    print(letters_new.index("b"))
    if "d" in letters_new:
        print(letters_new.index("d"))
    print(letters_new.count("n"))

    # sorting
    numbers = [3, 51, 5, 9, 7]
    numbers.sort()
    print(numbers)
    # numbers.sort(reverse=True)
    print(sorted(numbers, reverse=True))
    print(numbers)

    items = [
        ("Product1", 10),
        ("Product2", 9),
        ("Product3", 12),
    ]

    # def sort_item(item):
    #     return item[1]

    # items.sort(key=sort_item)
    # print(items)

    items.sort(key=lambda item: item[1])
    print(items)

    # prices = []
    # for item in items:
    #     prices.append(item[1])

    # using mapping and lamda
    prices = list(map(lambda item: item[1], items))
    # list comprehension
    prices = [item[1] for item in items]
    print(prices)
    # print(prices)
    # for item in x:
    #     print(item)

    # for item in items:
    #     if item[1] >= 10:
    #         print(item)

    filtered = list(filter(lambda item: item[1] >= 10, items))
    # list comprehension
    filtered = [item for item in items if item[1] >= 10]

    print(filtered)

    # zip function

    list1 = [1, 2, 3]
    list2 = [10, 20, 30]

    print(list(zip("abc", list1, list2)))

    # stack - last in - first out
    browsing_session = []
    browsing_session.append(1)  # adding always on the stack items
    browsing_session.append(2)
    browsing_session.append(3)
    browsing_session.pop()  # last added item will deleted
    if not browsing_session:  # checking if the stack is empty
        browsing_session[-1]  # get the item on top of the stack
    print(browsing_session)

    # queue
    queue = deque([])
    queue.append(1)
    queue.append(2)
    queue.append(3)
    queue.popleft()
    if not queue:
        print("empty")

    # tuples
    point = 1, 2, 3
    point_2 = (1, 2)
    point_3 = tuple([7, 9])  # convert a list to a tuple

    print(point)
    print(point_2)
    print(point_3)
    print(point[:1])
    x, y, z = point
    if 9 in point:
        print(True)
    else:
        print(False)

    # swapping variable
    # first by using a function
    x = 10
    y = 11

    def swap(a, b):
        temp = a
        a = b
        b = temp
        return a, b

    print(swap(x, y))
    # with one line
    x, y = y, x  # right side is tuple definition
    # and then unpacking the variables and assigning them
    # to x and y accordingly
    print("x", x)
    print("y", y)

    # array
    # arrays are used instead of lists then you need performance

    # i is typecode as you can look up by browsing it
    numbers = array("i", [1, 2, 3])

    # set
    numbers = [1, 1, 2, 3, 4]
    first = set(numbers)
    second = {1, 5}
    # second.add(5)
    # second.remove(5)
    # len(second)
    # print(uniques)
    print(first | second)  # return the union of both sets
    print(first & second)  # returns only the items that are in both
    print(first - second)  # return the first set minus the second set
    # returns items except that are either in the first or in the second
    # set but not both
    print(first ^ second)

    # dictionaries
    print("\n dictionaries: ")
    # point = {"x": 1, "y": 2}
    point = dict(x=1, y=2)
    print(point)
    print(point["x"])  # look up item by their key
    print(point["y"])
    point["z"] = 20
    point["y"] = 30
    print(point)
    if "a" in point:
        print(point["a"])
    print(point.get("a", 0))
    del point["x"]  # delete item "x"
    print(point)

    for x in point:
        print(x)

    for key in point:
        print(key, point[key])

    for x in point.items():
        print(x)

    for key, value in point.items():
        print(key, value)

    # list
    # values = []
    # for x in range(5):
    #     values.append(x * 2)

    # [expression for item in items]
    values = [x * 2 for x in range(5)]
    print(values)

    # dictionary
    # value_2 = {}
    # for x in range(5):
    #     values_2[x] = x * 2

    values_2 = {x: x * 2 for x in range(5)}
    print(values_2)

    # tuples
    values_3 = (x * 2 for x in range(5))
    print(values_3)
    for v in values_3:
        print(v)

    # generator object and they are iterable
    values = (x * 2 for x in range(1000))
    print("generator:", getsizeof(values))
    # print(values)
    # for x in values:
    #     print(x)



if __name__ == "__main__":
    main()
