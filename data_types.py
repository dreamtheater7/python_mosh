# !/usr/bin/env python3
# -*-coding;cp252-*-


def main():
    print("this this data types exercise.")

    v = 7
    print(v)
    print(id(v))
    x = v
    print(x)
    v = 9
    print(v)
    print(id(v))
    arr = [3, 5, 9]
    arr[0] = 7
    print(arr)
    tup = (3, 5, 9)
    #tup[0] = 7
    print(tup)

    def get_largest_number(numbers, n):
        numbers.sort()
        return numbers[-n:]

    numbers = [11, 77, 23, 9, 100, 21]
    print(numbers)
    largest = get_largest_number(numbers, 3)
    print(numbers)
    print(largest)

    print("variables reference objects.")

    v1 = 3
    v2 = 3

    print(f"address of v1 is {id(v1)}")
    print(f"address of v2 is {id(v2)}")

    v1 = 7

    print(f"address of v1 is {id(v1)}")

    arr = [100, 102, 103, 105, 107, 109, 111]

    print(f"address of arr is {id(arr)}")
    print(f"address of arr[0] is {id(arr[0])}")
    print(f"address of arr is {id(arr[1])}")
    print(f"address of arr is {id(arr[2])}")
    print(f"address of arr is {id(arr[3])}")
    print(f"address of arr is {id(arr[4])}")
    print(f"address of arr[5] is {id(arr[5])}")

    arr[0] = 109

    print(f"address of arr is {id(arr)}")
    print(f"address of arr[0] is {id(arr[0])}")

    print(hex(id(arr[0])))
    print(hex(id(arr[5])))

    a = 259
    b = 259
    print(id(a), id(b))
    print(a == b)

    class Point():
        def __init__(self, x: int, y: int):
            self.x = x
            self.y = y
            # print(f"self.x {self.x} address is {id(self.x)}")
            # print(f"self.y {self.y} address is {id(self.y)}")

        def __eq__(self, other):
            return self.x == other.x and self.y == other.y

        def __gt__(self, other):
            return self.x > other.x and self.y > other.y

        def draw(self):
            print(f"point ({self.x}) {id(self.x)}, ({self.y}) {id(self.y)}")

    point = Point(10, 20)
    other = Point(1, 2)
    print(point.draw())
    print(other.draw())

    print(f"point address is {id(point)}")
    print(f"other address is {id(other)}")

    print(point == other)
    print(point is other)
    print(point > other)


if __name__ == "__main__":
    main()
