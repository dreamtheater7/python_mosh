# !/usr/bin/env python3
# -*-coding:cp1252-*-

from abc import ABC, abstractmethod


def main():
    print("this is an example of inheritance from the lessons.")

    class InvalidOperationError(Exception):
        pass

    class Stream(ABC):
        def __init__(self):
            self.opened = False

        def open(self):
            if self.opened:
                raise InvalidOperationError("Stream already open.")
            self.opened = True

        def close(self):
            if not self.opened:
                raise InvalidOperationError("Stream is closed.")
            self.opened = False

        @abstractmethod
        def read(self):
            pass

    class FileStream(Stream):
        def read(self):
            print("reading data from a file.")

    class NetworkStream(Stream):
        def read(self):
            print("reading from the network.")

    class MemoryStream(Stream):
        def read(self):
            print("reading data.")


if __name__ == "__main__":
    main()  # this is main program.
