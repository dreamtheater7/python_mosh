# !/user/bin/env python3
# -*-coding:cp1252-*-
#from sales_modules import calc_shipping
# import ecommerce.sales
# import sys
from ecommerce.shopping.sales_modules import calc_tax
# from ecommerce import sales
from ecommerce.shopping import sales_modules



def main():
    print("this is an example of modules.")

    # print(sys.path)
    # ecommerce.sales.calc_tax()
    calc_tax()
    sales_modules.calc_shipping()
    print(dir(sales_modules))
    print(sales_modules.__name__)
    print(sales_modules.__package__)
    print(sales_modules.__file__)


if __name__ == "__main__":
    main()
