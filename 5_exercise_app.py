# !/usr/bin/env python3
# -*-coding:cp1252-*-

def main():
    print("this is exercise.")

    class Vehicle:

        discount = 0.8

        def __init__(self, title: str, speed: float, wheels: int, doors: int, price: float):
            assert speed >= 160, f"speed is {speed} too low."
            assert wheels > 1, f"wheels are {wheels} to few."
            assert doors > 2, f"doors are {doors} to few."

            self.model_name = title
            self.how_fast = speed
            self.number_of_wheels = wheels
            self.number_of_doors = doors
            self.price = price
            print(f"address of self.how_fast {id(self.how_fast)}")
            print(f"address of speed {id(speed)}")
            print(f"address of wheels {id(wheels)}")
            print(
                f"address of self.number_of_wheels {id(self.number_of_wheels)}")
            print(f"address of self {id(self)}")
            # print(f"address of __init__ {id(__init__())}")

        def set_drive(self):
            # self.how_fast = speed
            # print(self.how_fast)
            if self.how_fast > 310 and self.number_of_doors <= 3:
                print(f"the model {self.model_name} is a coupe")
            elif self.number_of_wheels == 2:
                print(f"model {self.model_name} is a motorbike.")
            elif self.how_fast < 310 and self.number_of_doors >= 3:
                print(f"the model {self.model_name} is a hatchback.")
            elif self.number_of_doors > 3:
                print(f"the model {self.model_name} is a limusine.")

        def calculate_new_price(self):
            self.price *= self.discount

    # it is only possible to pass arguments to the method
    # if constructor is defined.
    v1 = Vehicle("Lepard", 510, 4, 3, 53000.00)
    # print(v1.model_name)
    # print(v1.how_fast)
    # print(v1.number_of_wheels)
    # print(v1.number_of_doors)
    # print(v1.price)
    # v1.calculate_new_price()
    # print(v1.price)
    # without constructor
    # arguments have to be passed through method call
    # v1.set_drive()
    # v1.set_drive()
    print(f"address of v1 is {id(v1)}")
    print(f"address of Vehicle is {id(Vehicle)}")


if __name__ == "__main__":
    main()
