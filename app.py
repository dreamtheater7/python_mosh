# import math

# students_count = 1000
# rating = 4.99
# is_published = False
# course_name = "Python Programming"
# print(len(course_name))
# print(course_name)
# # print first 3 charchters
# print(course_name[:3])  # printing 0,1,2 without 3
# print(course_name[0:])
# print(course_name[:])

# # escape sequence \
# course_name_2 = "Progressive \"Music\""
# print(course_name_2)
# # escape sequence printing slash
# name = "John\\Johnny"
# print(f"name is {name}")

# first = "John"
# last = "Petrucci"
# # concatenation with +
# full = first + " " + last
# print("first name: " + str(first))
# print("last name: " + str(last))
# print("first name is " + str(first) + " and last name is " + str(last))
# print("full name is: " + str(full))
# # formated strings
# # it is an expression that will be evaluated on run time
# print(f"name is {first} {last}")
# print(f"name is {full}")

# full_version_2 = f"{first} {last}"
# print(full_version_2)

# course_name = "    Python Programming"
# print(course_name.upper())
# print(course_name.lower())
# print(course_name.title())
# print(course_name.strip())
# print(course_name.lstrip())
# print(course_name.rstrip())
# # find returns index
# print(course_name.find("Pro"))
# print(course_name.replace("Pro", "pro"))
# print("pro" in course_name)
# print("pro" not in course_name)
# print("g" in course_name and "j" in course_name)

# # integer number
# x = 1
# # float number
# x = 1.1
# # complex numbers
# # x =  a+bi - where i is the imaginary number.
# x = 1 + 2j

<<<<<<< HEAD
# dicision floating number
print(10 / 3)
# division integer number
print(10 // 3)
# exponent (left to the power of right)
print(10**3)
=======
# # dicision floating number
# print(10 / 3)
# # division integer number
# print(10 // 3)
# # exponent (left to the power of right)
# print(10**3)
>>>>>>> a101646ff78b40301fbe256795a102c6fcebafac

# print(round(2.9))
# print(abs(-2.9))

# # using math module
# print(math.ceil(2.2))

# Type Conversion
# x = input("x: ")
# print(type(x))
# y = int(x) + 1
# print(f"y is {y}")
# in python build conversions:
# int(x), float(x), bool(x), str(x)

# temperature = 35
# if temperature > 30:
#     print("it is summer")


# age = 23
# if age >= 18:
# message = "Eligible"
# print("Eligible")
# else:
# message = "Not Eligible"
# print("Not Eligible")

# message = "Eligible" if age >= 18 else "Not Eligible"
# print(message)


# high_income = False
# good_credit = True

# if high_income and good_credit:
#     print("Eligible")
# else:
#     print("Not Eligible")

# age should be between 18 and 65
# age = 23
# if age >= 18 and age < 65:
#     print("Eligible")

# chaining comparision operators
# if 18 <= age < 65:
#     print("Eligible")


# for number in range(0, 10, 2):
#     print("Attempt", number + 1, (number + 1) * ".")

# for i in range(5):
#     for j in range(3):
#         print(f"{i}{j}")

# number = 100
# while number > 0:
#     print(number)
#     number //= 2

# command = ""
# while command.lower() != "quit":
#     command = input("> ")
#     print("ECHO", command)


# def greet(first_name, last_name):
#     print(f"Hi {first_name} {last_name}")
#     print("Welcome aboard!")


# greet("John", "Petrucci")


# def get_greeting(name):
#     return f"{name} is the land of my choice!"


# message = get_greeting("Ireland")
# # print(message)
# file = open("content.txt", "w")
# file.write(message)

# print("Progressive Music")
# command = input(">")
# print(command)

# xargs - multiple arguments with single parameter
def multiple(*numbers):
    res = 1
    for i in range(len(numbers)):
        res = res * numbers[i]
    return res


def multiple_2(*numbers):
    res = 1
    for number in numbers:
        res = res * number
    return res


result = multiple(2, 3, 4, 5)
print(f"result is {result}")
result_2 = multiple_2(2, 3, 4, 5)
print(f"result_2 is {result_2}")


def save_user(**user):
    print(user)
    print(user["id"])


save_user(id=1, name="John", age=22)

name = "Mike"


def func(name):
    #global name
    print(f"the local variable name is {name}")

    def int_func(name):
        print(f"nested variable name {name}")

    int_func("James")


func("John")
print(f"the global variable name is {name}")
