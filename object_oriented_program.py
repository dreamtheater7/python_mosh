# !/user/bin/env python3
# -*-coding:cp1252-*-


def main():
    # print("this is a object oriented program.")

    class Item:
        pay_rate = 0.8  # The pay rate after 20% discount

        # definition of the contructor.
        def __init__(self, name: str, price: float, quantity=0):
            # Run validations to the recieved arguments.
            assert price >= 0, f"Price {price} is not greater zero."
            assert quantity >= 0, f"Quantity {quantity} is not greater zero."

            # Assign to self object
            # print(f"Constructor for {name}!")
            self.name = name
            self.price = price
            self.quantity = quantity
            # print(f"An instance created: {name}, {price} and {quantity}")]
            # print(
            #     f"addresses of name {id(name)} price {id(price)} and quantity {id(quantity)}")
            # print(f"address of self: {id(self)}")

        # def calculate_total_price(self, item_price, item_quantity):
        #     return item_price * item_quantity
        def calculate_total_price(self):
            return self.price * self.quantity

        def apply_discount(self):
            # overwrite the old price
            self.price = self.price * self.pay_rate

    # creating instance item1 of Object Item
    item1 = Item("Phone", 100, 5)
    # assigning attributes to instance
    # item1.name = "Phone"
    # item1.price = 100
    # item1.quantity = 5
    # print(item1.price, item1.quantity)
    # print(item1.calculate_total_price())
    print(f"price for phone before discount: {item1.price}")
    item1.apply_discount()
    print(f"price for phone after discount: {item1.price}")

    # creating instance item1 of Object Item
    item2 = Item("Laptop", 1000, 3)
    # assigning attributes to instance
    # item2.name = "Laptop"
    # item2.price = 1000
    # item2.quantity = 3
    # item2.has_numpad = False
    # print(item2.price, item2.quantity)
    # print(item2.calculate_total_price(item2.price, item2.quantity))
    # print(item2.calculate_total_price())
    print(f"price for laptop before discount: {item2.price}")
    item2.pay_rate = 0.7
    item2.apply_discount()
    print(f"price for laptop after discount: {item2.price}")

    # print(type(item1))
    # print(f"address of Item: {id(Item)}")
    # address of item1 and self is the same
    # print(f"address of item1 is: {id(item1)}")
    # print(f"address of item1.name: {id(item1.name)}")
    # print(f"address of item1.price: {id(item1.price)}")
    # print(f"address of item1.quantity: {id(item1.quantity)}")
    # print("*****************************")
    # print(f"address of Item: {id(Item)}")
    # address of item2 and self is the same
    # print(f"address of item2 is: {id(item2)}")
    # print(f"address of item2.name: {id(item2.name)}")
    # print(f"address of item2.price: {id(item2.price)}")
    # print(f"address of item2.quantity: {id(item2.quantity)}")
    # print("******************************")
    # print(Item.pay_rate)
    # print(id(Item.pay_rate))
    # print(item1.pay_rate)
    # print(id(item1.pay_rate))
    # print(item2.pay_rate)
    # print(id(item2.pay_rate))
    # print(Item.__dict__)
    # print(item1.__dict__)
    # item3 = Item()
    # item4 = Item()
    # item5 = Item()

    item1 = Item("Phone", 100, 1)
    item2 = Item("Laptop", 1000, 3)
    # item3 = Item("Cable", 10, 5)
    # item4 = Item("Mouse", 50, 5)
    # item5 = Item("Keyboard", 75, 5)

    print("progressive music")
    name = input("enter name: ")
    print(f"your name is: {name}")


if __name__ == "__main__":
    main()












